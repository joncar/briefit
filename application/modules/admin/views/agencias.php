<?= $output ?>
<script>
    function triggerDirection(){
        if (mapa.map) { 
            console.log("Buscando direccion");
            var geocoder = new google.maps.Geocoder();
            var address = $(this).val();
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    mapa.map.setCenter(results[0].geometry.location);
                    mapa.marker.setPosition(results[0].geometry.location);
                    mapa.map.setZoom(16);
                    console.log(results[0].geometry.location);
                    $("#field-mapa").val("("+results[0].geometry.location.lat()+","+results[0].geometry.location.lng()+")");
                }
            });
        } 
    }
    $(document).on("change","#field-direccion",triggerDirection);
    $(document).on("ready",function(){
       //triggerDirection();
    });
</script>
