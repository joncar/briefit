<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        const __AGENCIA_GRUPOID__ = 2;
        const __CLIENTE_GRUPOID__ = 3;
        function __construct() {
            parent::__construct();
        }
        
        function tipos_clasificacion(){
            $crud = $this->crud_function("","");
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function clasificaciones(){
            $crud = $this->crud_function("","");
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function bancos(){
            $crud = $this->crud_function("","");
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function agencias($x = '',$y = ''){            
            $this->as['agencias'] = 'user';
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));                                    
            $crud->field_type('password','password')
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 ->field_type('fecha_registro','hidden',date("Y-m-d"))
                 ->field_type('status','hidden',1)
                 ->field_type('certificacion','dropdown',array('1'=>'En tramite','2'=>'A','3'=>'AA','4'=>'AAA','5'=>'E'))
                 ->field_type('mapa','map');            
            $crud->set_primary_key('user','user_group');
            $crud->set_relation('id','user_group','grupo');
            $crud->set_relation('clasificaciones_id','clasificaciones','clasificacion_nombre');
            $crud->set_relation_dependency('clasificaciones_id','tipos_clasificacion_id','tipos_clasificacion_id');
            $crud->where('grupo',self::__AGENCIA_GRUPOID__);
            $crud->display_as('foto','Logo')
                 ->display_as('clasificaciones_id','Clasificación de la agencia')
                 ->display_as('tipos_clasificacion_id','Tipo de clasificación de agencia')
                 ->display_as('nombre_comercial','Nombre comercial (En caso de ser diferente a la razon social)')
                 ->display_as('bancos_id','Cuenta bancaria (Banco)')
                 ->display_as('tipo_cuenta','Tipo de cuenta bancaria')                    
                 ->display_as('#cuenta bancaria','#Cuenta');
            $crud->required_fields('nombre','apellido','email','password','puesto','razon_social','nombre_comercial','rfc','direccion','mapa','foto','tipos_clasificacion_id','clasificaciones_id','certificacion');
            $crud->fields('nombre','apellido','email','password','puesto','razon_social','nombre_comercial','rfc','direccion','mapa','foto','fecha_registro','fecha_actualizacion','status','bancos_id','tipo_cuenta','cuenta','tipos_clasificacion_id','clasificaciones_id','certificacion');
            $crud->unset_columns('password','');
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');            
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){                
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::__AGENCIA_GRUPOID__));                                
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->unset_delete();
            $crud->columns('foto','nombre','apellido','email','rfc');            
            $output = $crud->render();
            $output->output = $this->load->view('agencias',array('output'=>$output->output),TRUE);
            $this->loadView($output);
        }
        
        function clientes($x = '',$y = ''){            
            $this->as['clientes'] = 'user';
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));                                    
            $crud->field_type('password','password')
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 ->field_type('fecha_registro','hidden',date("Y-m-d"))
                 ->field_type('status','hidden',1)
                 ->field_type('marcas_oficiales','tags');
            
            $crud->set_primary_key('user','user_group');
            $crud->set_relation('id','user_group','grupo');
            $crud->where('grupo',self::__CLIENTE_GRUPOID__);
            $crud->display_as('foto','Logo')
                 ->display_as('bancos_id','Cuenta bancaria (Banco)')
                 ->display_as('tipo_cuenta','Tipo de cuenta')
                 ->display_as('cuenta','#Cuenta bancaria')
                 ->display_as('marcas_oficiales','Coloque sus marcas o productos separados por coma (,)');
            $crud->fields('nombre','apellido','email','password','puesto','razon_social','rfc','direccion','foto','fecha_registro','fecha_actualizacion','status','bancos_id','tipo_cuenta','cuenta','marcas_oficiales');
            $crud->required_fields('nombre','apellido','email','password','puesto','razon_social','rfc','direccion','foto','marcas_oficiales');
            $crud->unset_columns('password','');
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');            
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){                
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::__CLIENTE_GRUPOID__));                                
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->unset_delete();
            $crud->columns('foto','nombre','apellido','email','rfc');
            $output = $crud->render();
            $this->loadView($output);
        }
        
    }
?>
