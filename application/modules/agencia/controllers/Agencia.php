<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Agencia extends Panel{
        const __GRUPOID__ = 2;
        function __construct() {
            parent::__construct();
        }
        
        function agencias($x = '',$y = ''){            
            $this->as['agencias'] = 'user';
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password');
            $crud->display_as('foto','Logo');
            $crud->fields('nombre','apellido','email','password','puesto','razon_social','rfc','direccion','foto');
            $crud->unset_columns('password','');
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            $crud->unset_print()
                 ->unset_export();            
            $crud->where('user.id',$this->user->id);
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::__GRUPOID__));            
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->columns('foto','nombre','email','status');
            $output = $crud->render();
            $this->loadView($output);
        }
        
    }
?>
