<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?= empty($title) ? 'Monalco' : $title ?></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />		

        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700italic,700,400italic%7CLato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic%7COpen+Sans:Open+Sans:400,800italic,800,700italic,700,600italic,600,400italic,300italic,300%7CHandlee">
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/megafish.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/superfish.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/flexslider.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery.qtip.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery-supersized.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery.nivo.slider.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/fancybox/jquery.fancybox.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/fancybox/helpers/jquery.fancybox-buttons.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/revslider/layers.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/revslider/settings.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/revslider/navigation.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/base.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/responsive.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/retina.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/editor.css"/> 
        <script>
            var URL = '<?= base_url() ?>';
        </script>
        <script type="text/javascript" src="<?= base_url() ?>js/template/jquery.min.js"></script>
    </head>

    <body>        
        <?= $this->load->view($view) ?>
        <?= $this->load->view('includes/template/footer') ?>
        <?= $this->load->view('includes/template/scripts') ?>
    </body>

</html>
